import joblib

def load_coeffs_model(path_model):
    model = joblib.load(path_model)

    return model.coef_[0], model.intercept_

def create_predict_function():
    coeffs = load_coeffs_model("./model")

    thetas, bias = coeffs[0], coeffs[1]

    thetas_str = "{"
    n_thetas = len(thetas)
    for i in range(n_thetas - 1):
        thetas_str += str(thetas[i])
        thetas_str += ","
    thetas_str += str(thetas[n_thetas - 1])
    thetas_str += "}"

    predict_function = f"""
    #include <stdio.h>
    #include <stdlib.h>
    float linear_regression_prediction(float* features, int n_feature)
    {{
        float res = {bias[0]};
        int n_thetas = {n_thetas};
        float thetas[] = {thetas_str};
        for (int i = 0; i < n_thetas; i++)
        {{
            res += features[i] * thetas[i];
        }}
        return res;
    }}
    """

    return predict_function

def create_main_function():
    main_function = """
        int main(int argc, char *argv[])\n
        {\n
            int n_features = argc - 1;\n
            char *feature_1 = argv[1];\n
            char *feature_2 = argv[2];\n
            float array[] = { atof(feature_1), atof(feature_2) };\n
            printf(\"%f\", linear_regression_prediction(array, n_features));\n
        }
        """
    return main_function

def main():
    with open("linear_regression.c", "w") as f:
        f.write(create_predict_function())
        f.write(create_main_function())

if __name__ == '__main__':
    main()