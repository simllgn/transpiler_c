#! /bin/sh

echo "*** Create Virtual Environment ***"
python -m venv venv

echo "*** Activate Virtual Environment ***"
. venv/bin/activate

echo "*** Install dependencies ***"
pip install -r requirements.txt
