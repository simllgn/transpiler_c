# Setup Environment

    . setup.sh

# Train model

The model will be trained on tumors.csv. Run the following command:

    python train\_model.py

# Launch Transpiler

This section will produce linear\_regression.c file. Run the following command:

    python transpiler.py

# Clean Repository

You can delete all produced files.

    make clean
