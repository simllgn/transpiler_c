import joblib
import pandas as pd 
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression

data = pd.read_csv('./tumors.csv')

X = data.iloc[:, :2].values
y = data.iloc[:, 2:].values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)

model = LinearRegression().fit(X_train, y_train)

filename = 'model'
joblib.dump(model, open(filename, 'wb'))
